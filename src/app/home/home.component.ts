import { Component, OnInit } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[OfertasService]
})
export class HomeComponent implements OnInit {

  public ofertas:Array<Oferta>

  constructor(private ofertasService:OfertasService) { }

  ngOnInit() {
    // this.ofertas = this.ofertasService.getOfertas();
    // this.ofertasService.getOfertas2()
    // .then( (ofertas:Array<Oferta>) =>{
    //   console.log('deu certo')
    //   this.ofertas = ofertas
    // }) //Then para onFulfilled(deu certo)
    // .catch((onFailure: any)=>{
    //   console.log(onFailure)
    // }); //Catch para pegar deu erro (onFailure)
    this.ofertasService.getOfertasDestaque()
      .then((retorno:Oferta[])=>{
        this.ofertas = retorno
      })
      .catch((paramn:any)=>{
        console.log(paramn)
      })
  }

}

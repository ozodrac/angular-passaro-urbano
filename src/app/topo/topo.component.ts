import { Component, OnInit } from '@angular/core';
import { OfertasService } from '../ofertas.service';

@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css'],
  providers:[OfertasService]
})
export class TopoComponent implements OnInit {

  public ofertas: Observable<Oferta[]>;

  constructor(private ofertasService:OfertasService) { }

  ngOnInit() {
  }
  // public pesquisa(event: Event):void{
  //   let params = (<HTMLInputElement>event.target).value
  //   console.log(params)
  // }
  public pesquisa(termoPesquisado: string):void{
    this.ofertas = this.ofertasService.pesquisaByName(termoPesquisado);

    this.ofertas.subscribe((oferta:Oferta[])=>console.log(oferta))
  }
}

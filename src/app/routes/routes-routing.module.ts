import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "../home/home.component";
import { RestaurantesComponent } from "../restaurantes/restaurantes.component";
import { DiversaoComponent } from "../diversao/diversao.component";
import { LeituraComponent } from "../leitura/leitura.component";
import { ComoUsarComponent } from '../leitura/como-usar/como-usar.component';
import { OndeFicaComponent } from '../leitura/onde-fica/onde-fica.component';

const routes: Routes = [
  { path: 'restaurante', component: RestaurantesComponent },
  { path: 'diversao', component: DiversaoComponent },
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'leitura/:id', component: LeituraComponent,
    children:[
      { path: '', component: OndeFicaComponent },
      { path:'onde-fica', component: OndeFicaComponent },
      { path:'como-usar', component: ComoUsarComponent },
    ]
  },
  { path: `restaurante/:id`, redirectTo: '/leitura/:id', pathMatch: 'full' },
  { path: 'diversao/:id', redirectTo: '/leitura/:id', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutesRoutingModule { }

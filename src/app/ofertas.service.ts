import { Injectable } from '@angular/core';
import { Oferta } from './shared/oferta.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()

export class OfertasService {

    constructor(private http:HttpClient) {

    }

    public ofertas: Oferta[];
    private apiUrl: string = "http://localhost:3000/";


    public getOfertas(): Promise<Oferta[]>{
      //Efetuar requisiçao http à api
      //retornar uma promise de Array<Oferta>
      return this.http.get(this.apiUrl)
      .toPromise()
      .then((resposta: any) => resposta)
    }

    public getOfertasDestaque(): Promise<Array<Oferta>>{
      return this.http.get(`${this.apiUrl}ofertas?destaque=true`)
      .toPromise().then( (retorno:any)=>retorno)
    }

    public getOfertasPorCategoria(categoria:string): Promise<Oferta[]>{
      return this.http.get(`${this.apiUrl}ofertas?categoria=${categoria}`)
        .toPromise()
        .then((resposta: any) => resposta )
    }

    public getOfertaPorId(id:string):Promise<Oferta>{
      return this.http.get(`${this.apiUrl}ofertas?id=${id}`)
      .toPromise()
      .then((response:any)=>response)
    }

    public getComoUsarId(id:number): Promise<string>{
      return this.http.get(`${this.apiUrl}como-usar?id=${id}`)
      .toPromise()
      .then((retorno:any)=>retorno[0].descricao)
    }

    public getOndeFicaId(id:number): Promise<string>{
      return this.http.get(`${this.apiUrl}onde-fica?id=${id}`)
      .toPromise()
      .then((retorno:any)=>retorno[0].descricao)
    }

    public pesquisaByName(termo:string): Observable<Oferta[]>{
      return  this.http.get(`${this.apiUrl}ofertas?descricao_oferta_like=${termo}`)
              .pipe(map((resposta:any)=> resposta ))
    }

}

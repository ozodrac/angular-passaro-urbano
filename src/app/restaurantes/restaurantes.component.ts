import { Component, OnInit } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.css'],
  providers:[OfertasService]
})
export class RestaurantesComponent implements OnInit {

  public ofertas: Oferta[];
  constructor(private ofertaService:OfertasService,private router: Router) { }

  ngOnInit() {
    let url:any = this.router.url.split('/')
    url = url[1];
    this.ofertaService.getOfertasPorCategoria(url)
    .then((response:Oferta[])=>{
      this.ofertas = response
    })
    .catch((param:any)=>{
      console.log(param)
    })
  }

}

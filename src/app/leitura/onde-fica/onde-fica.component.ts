import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OfertasService } from 'src/app/ofertas.service';

@Component({
  selector: 'app-onde-fica',
  templateUrl: './onde-fica.component.html',
  styleUrls: ['./onde-fica.component.css']
})
export class OndeFicaComponent implements OnInit {

  private id:number;
  public ondeFica:string;

  constructor(private route:ActivatedRoute,private ofertService:OfertasService) {
    this.id= this.route.parent.snapshot.params.id
   }

  ngOnInit() {
    this.ofertService.getOndeFicaId(this.id)
    .then((descricao)=>{
      this.ondeFica = descricao
    })
  }

}

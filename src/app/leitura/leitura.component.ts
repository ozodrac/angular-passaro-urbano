import { Component, OnInit, OnDestroy } from '@angular/core';
import { OfertasService } from '../ofertas.service';
import { Oferta } from '../shared/oferta.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-leitura',
  templateUrl: './leitura.component.html',
  styleUrls: ['./leitura.component.css'],
  providers:[OfertasService]
})
export class LeituraComponent implements OnInit,OnDestroy {

  public oferta:Oferta;
  public id:string;


  constructor(private ofertaService:OfertasService,private route:ActivatedRoute) {
    // this.id = this.route.snapshot.params.id; //snapshot
    // this.route.params.subscribe((params:any)=>{ console.log(params)}) //subscribe permite um callback
    this.route.params.subscribe(
      (next: any) => {this.id = next.id},
      (error:any)=>{console.log(error)},
      ()=>{console.log('concluido')}
      )
  }

  ngOnInit() {
    this.ofertaService.getOfertaPorId(this.id)
    .then((result:any)=>{
      this.oferta = result.shift()
    })
    .catch((erros:any)=>{console.log(erros)});


  }

  ngOnDestroy(){
  }

}

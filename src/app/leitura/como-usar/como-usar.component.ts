import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OfertasService } from 'src/app/ofertas.service';

@Component({
  selector: 'app-como-usar',
  templateUrl: './como-usar.component.html',
  styleUrls: ['./como-usar.component.css']
})
export class ComoUsarComponent implements OnInit {

  private id:number;
  public comoUsar:string;

  constructor(private route: ActivatedRoute, private ofertaService: OfertasService) {
    this.id = this.route.parent.snapshot.params.id;
  }

  ngOnInit() {
    this.ofertaService.getComoUsarId(this.id)
    .then((descricao)=>{
      this.comoUsar = descricao
    })
  }

}
